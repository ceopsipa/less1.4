FROM debian:9 as build
ENV LUAJIT_LIB=/usr/local/LuaJIT/lib
ENV LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.0
RUN apt update && apt install -y wget gcc make libpcre++-dev zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.6.0.tar.gz && tar xvfz nginx-1.6.0.tar.gz
RUN wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz && tar -zxvf LuaJIT-2.0.5.tar.gz && cd LuaJIT-2.0.5 && make install PREFIX=/usr/local/LuaJIT
RUN wget https://github.com/openresty/lua-nginx-module/archive/v0.10.13.tar.gz && tar zxvf v0.10.13.tar.gz -C /tmp
RUN cd nginx-1.6.0 && ./configure --with-ld-opt=-Wl,-rpath,/usr/local/LuaJIT/lib --add-module=/tmp/lua-nginx-module-0.10.13 && make && make install

FROM debian:9
ENV LUAJIT_LIB=/usr/local/LuaJIT/lib
ENV LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.0
RUN apt update && apt install -y libssl-dev
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/LuaJIT /usr/local/LuaJIT
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]