# less1.4

compile nginx with modules

# Build nginx 1.6.0 with Lua and run

```sh
docker build -t nginx_custom:v1 .
docker run -d -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf -p 80:80 nginx_custom:v1
```